<?php

namespace amitours\cookie_policy\widgets;

use Yii;
use amitours\cookie_policy\models\Cookie;
use yii\base\Widget;

class Table extends Widget {
    public $cookie_set;
	
	public function run() {
		$model = new Cookie();
		$model->cookie_set = $this->cookie_set;
		return $model->getHtmlTable() . $model->getOptOutText();
    }
}

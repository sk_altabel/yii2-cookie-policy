<?php
namespace amitours\cookie_policy\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Cookie extends Model
{
	public $config;
	public $cookie_set;
	
	public function init()
	{
		$this->config = require(__DIR__ . '/../config/config.php');
	}
	
	public function getConfigItem($key)
	{
		$result = [];
		if (isset($this->config[$key])) {
			$result = $this->config[$key];
		}
		return $result;
	}
	
	public function getConfigCookies()
	{
		return $this->getConfigItem('cookies');
	}
	
	public function getConfigTitles()
	{
		return $this->getConfigItem('titles');
	}
	
	public function getConfigDescriptions()
	{
		return $this->getConfigItem('descriptions');
	}
	
	public function getConfigCookiesByTitleCode($title_code)
	{
		return array_filter($this->configCookies, function ($cookie) use ($title_code) {
			return ($title_code == $cookie['title_code']) ? true : false;
		});
	}
	
	public function getSelectedCookies()
	{
		$result = [];
		if ($this->cookie_set) {
			$index = 0;
			foreach ($this->cookie_set as $key => $value) {
				if (is_int($key)) {
					$title_code = $value;
					$config_title_cookies = $this->getConfigCookiesByTitleCode($title_code);
					foreach ($config_title_cookies as $config_title_cookie) {
						$result[$index] = $config_title_cookie;
						$index++;
					}
				} else {
					$title_code = $key;
					$title_cookie_list = (is_array($value) ? $value : [$value]);
					
					$config_title_cookies = $this->getConfigCookiesByTitleCode($title_code);
					$grouped_config_cookies = ArrayHelper::index($config_title_cookies, 'name');
					
					foreach ($title_cookie_list as $i => $name) {
						if (isset($grouped_config_cookies[$name])) {
							$result[$index] = $grouped_config_cookies[$name];
							$index++;
						}
					}
				}
			}
		}
		return $result;
	}
	
	public function getSelectedCookiesGroupedByTitle()
	{
		$selected_cookies = $this->getSelectedCookies();
		return ArrayHelper::index($selected_cookies, null, 'title_code');
	}
	
	public function getCookieDescriptionData($cookie)
	{
		$result = [
			'description_key' => 'none',
			'description' => '',
		];
		
		if (isset($cookie['description'])) {
			$result = [
				'description_key' => isset($cookie['description_key']) ?
					$cookie['description_key']
					: ($cookie['title_code'] . '-' . $cookie['name'] . '-u')
				,
				'description' => $cookie['description'],
			];
		} else if (isset($this->configDescriptions[$cookie['title_code']][$cookie['name']])) {
			$result = [
				'description_key' => $cookie['title_code'] . '-' . $cookie['name'],
				'description' => $this->configDescriptions[$cookie['title_code']][$cookie['name']],
			];
		} else if (isset($this->configDescriptions[$cookie['title_code']][0])) {
			$result = [
				'description_key' => $cookie['title_code'] . '-0',
				'description' => $this->configDescriptions[$cookie['title_code']][0],
			];
		}
		return $result;
	}
	
	public function getSelectedCookiesGroupedByTitleAndDestination()
	{
		$result = [];
		$grouped_by_title = $this->getSelectedCookiesGroupedByTitle();
		
		foreach ($grouped_by_title as $title_code => $cookies) {
			foreach ($cookies as $index => $cookie) {
				$description_data = $this->getCookieDescriptionData($cookie);
				$cookie_with_desc = array_merge($cookie, $description_data);
				$result[$title_code][$description_data['description_key']][] = $cookie_with_desc;
			}
		}
		return $result;
	}
	
	public function getHtmlTableStart()
	{
		return '
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>' . Yii::t('cookie_policy', 'Cookie') . '</th>
					<th>' . Yii::t('cookie_policy', 'Title') . '</th>
					<th>' . Yii::t('cookie_policy', 'Description') . '</th>
				</tr>
			</thead>
			<tbody>
		';
	}
	
	public function getGroupedCookieCounts($desc_data) {
		$result = [
			'title_count' => 0,
			'desc_counts' => [],
		];
		foreach ($desc_data as $desc_key => $data) {
			$data_count = count($data);
			$result['title_count'] += $data_count;
			$result['desc_counts'][$desc_key] = $data_count;
		}
		return $result;
	}
	
	public function getHtmlTableContent()
	{
		$result = '';
		$grouped_selected_cookies = $this->getSelectedCookiesGroupedByTitleAndDestination();
		// file_put_contents('test.txt', print_r($grouped_selected_cookies, true));
		foreach ($grouped_selected_cookies as $title_code => $desc_data) {
			$grouped_cookie_counts = $this->getGroupedCookieCounts($desc_data);
			$desc_index = 0;
			foreach ($desc_data as $description_key => $cookies) {
				foreach ($cookies as $cookie_index => $cookie) {
					$result .= '<tr>';
					
					$result .= Html::tag('td', $cookie['name']);
					
					if ($desc_index == 0 && $cookie_index == 0) {
						$title_name = (isset($this->configTitles[$title_code]) ? 
							$this->configTitles[$title_code]
							: ''
						);
						$result .= Html::tag('td', $title_name, [
							'rowspan' => $grouped_cookie_counts['title_count'],
						]);
					}
					if ($cookie_index == 0) {
						$result .= Html::tag('td', $cookie['description'], [
							'rowspan' => $grouped_cookie_counts['desc_counts'][$description_key],
						]);
					}
					$result .= '</tr>';
				}
				$desc_index++;
			}
		}
		return $result;
	}
	
	public function getHtmlTableEnd()
	{
		return '
			</tbody>
		</table>
		';
	}
	
	public function getHtmlTable()
	{
		return $this->getHtmlTableStart()
			. $this->getHtmlTableContent()
			. $this->getHtmlTableEnd()
		;
	}
	
	public function getOptOutText()
	{
		$html = '<p>';	
		$selected_cookies = $this->getSelectedCookiesGroupedByTitle();
		foreach (['plugins', 'links'] as $type) {
			${'opt_out_' . $type} = '';
			foreach ($selected_cookies as $index => $cookie) {
				if (isset($this->config['opt_out'][$type][$index])) {
					${'opt_out_' . $type} .= (${'opt_out_' . $type} ? ', ' : '') . '<a href="' . $this->config['opt_out'][$type][$index] . '" rel="noopener noreferrer" target="_blank">' . $this->config['titles'][$index] . '</a>';
				}
			}
		}
		if ($opt_out_plugins) {
			$html .= 'Opt-out from 3rd party cookies is possible with following plugins: ' . $opt_out_plugins . '.';
		}
		if ($opt_out_links) {
			$html .= ($html ? '<br>' : '') . 'Also opt-out from 3rd party cookies is possible by checking "Opt-out" on the following pages: ' . $opt_out_links . '.';
		}
		$html .= '</p>';
		return $html;
	}
}

# Yii2 Cookie Policy

Yii2 Cookie Policy is a package that contains descriptions of cookies that used on amitours project websites and give the possibility easy to display only cookie descriptions that used in your current project by the widget.

### Installing

Update composer.json of your project:

```
{
	"require": {
		"vladimirsmaksimovs/yii2-cookie-policy": "dev-master"
	},
	"repositories":[
		{
			"type": "vcs",
			"url": "https://VladimirsMaksimovs@bitbucket.org/VladimirsMaksimovs/yii2-cookie-policy.git"
		}
	]
}
```

Then run

```
composer require vladimirsmaksimovs/yii2-cookie-policy:dev-master
```

### Draw Cookie Description table

To get Cookie Description table you need to add the following code:

```
echo \amitours\cookie_policy\widgets\Table::widget([
	'cookie_set' => [
		'<title_code>', // to display all cookies with certain title
		'<title_code2>' => [ // you can specify only the part of cookies with certain title
			'<cookie_1_name>',
			'<cookie_2_name>',
			...
		],
		'<title_code3>' => '<cookie_2_name>', // you can specify only one cookie of title title
	],
]);
```

The list of cookies you can find in the config file of package:

```
cd vendor/vladimirsmaksimovs/yii2-cookie-policy/src/config/config.php
```

Thank You!
